<?php


namespace App\Http\Controllers;
use DB;
 use App\Post;

class PostController extends Controller
{


    public function show($slug){

//        $post = \DB::table('posts')->where('slug',$slug)->first();
//        dd($post);
        $post = Post::where('slug', $slug)->firstOrFail();

//        $posts=[
//            'my-first-post' => 'Hello, this is my fist blog post',
//            'my-second-post' => 'Now I am getting the hang of this blogging thing.'
//        ];
//
//        if (! array_key_exists($post,$posts)){
//            abort(404,'Sorry post was not found.');
//        }
//
        return view('posts',[
            'post' => $post
        ]);
    }
}
